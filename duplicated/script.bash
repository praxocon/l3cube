#!/bin/bash

CWD=$(pwd)
DEBUG=0
GLOBAL_BASE="./.duplicated"
if [ ! -d "$GLOBAL_BASE" ]; then
	mkdir "$GLOBAL_BASE"
fi
# MAIN TABLE SCHEMA
# INODE MD5
GLOBAL_MAIN_TABLE="$GLOBAL_BASE/.duplicated-main"
# DUPLICATE TABLE SCHEMA
# MD5 INODE1 INODE2 ...
GLOBAL_DUPLICATE_TABLE="$GLOBAL_BASE/.duplicated-dup"
if [ ! -f "$GLOBAL_MAIN_TABLE" ]; then
	touch "$GLOBAL_MAIN_TABLE"
fi
if [ ! -f "$GLOBAL_DUPLICATE_TABLE" ]; then
	touch "$GLOBAL_DUPLICATE_TABLE"
fi

# while read -d $'\0' BASE_DIR; do
while read BASE_DIR; do
	if [ ! -d "$BASE_DIR/.duplicated" ]; then
		mkdir "$BASE_DIR/.duplicated"
	fi
	
	NAMEFILE=$BASE_DIR/.duplicated/names.txt
	OLDNAMEFILE=$BASE_DIR/.duplicated/names.txt.old

	if [ $DEBUG -eq 1 ]; then
		echo "inside script.sh"
	fi

	if [ ! -f "$OLDNAMEFILE" ]; then
		touch "$OLDNAMEFILE" 
	fi

	find "$BASE_DIR" -maxdepth 1 -type f ! -name names.txt ! -name \
		names.txt.old -exec stat --format '%Y %i %n ' '{}' \; >> "$NAMEFILE"

	ORIGINAL_IFS=$IFS
	IFS=$'\n'
	for x in $( diff "$OLDNAMEFILE" "$NAMEFILE" --unchanged-line-format="" \
		--old-line-format="" --new-line-format="%L" ); do

		IFS=$ORIGINAL_IFS
		# obtaining values of inode number and md5 of new files
		read mod ino name <<< $(echo "$x")
		md5=$(md5sum "$name" | sed 's/^\([^ ]*\) \(.*\)$/\1/')
		printf -v line '%-15s%-45s %s' $ino $md5 "$name"

		OLD_INO="$(grep "$ino" "$GLOBAL_MAIN_TABLE")"
		MATCH_MD5="$(grep "$md5" "$GLOBAL_MAIN_TABLE")"
		MATCHING_INODE=0
		MD5_DUPLICATE=0
		INODE_EXISTS=0
		if [ "$MATCH_MD5" != "" ]; then
			MD5_DUPLICATE=1
			MATCHING_INODE=$(echo "$MATCH_MD5" | sed 's/^\([^ ]*\) \(.*\)$/\1/')
		fi
		if [ "$OLD_INO" != "" ]; then
			INODE_EXISTS=1
		fi

		if [ $INODE_EXISTS == 1 ]; then 
			# as this is existing it may be already in duplicate of 
			# some other file
			NDUP=$(grep "$ino" "$GLOBAL_DUPLICATE_TABLE" | wc -w)
			if [ $NDUP == 2 ]; then
				# if number of duplicates are two then other file is 
				# no longer a duplicate file
				sed -i "/$ino/d" "$GLOBAL_DUPLICATE_TABLE"
			fi
			sed -i "s/$ino //g" "$GLOBAL_DUPLICATE_TABLE"

			# delete record from old file as replacement has been 
			# already been taken care of
			sed -i "/$ino/d" $OLDNAMEFILE
			if [ $MD5_DUPLICATE != 1 ]; then 
				sed -i "/$ino/c\\$line" $GLOBAL_MAIN_TABLE
			else 
				# if this is a duplicate file and for if below:
				# if matching inode not in duplicate table then add
				# matching existing inode to duplicate table
				if [ $(grep "$MATCHING_INODE" "$GLOBAL_DUPLICATE_TABLE") == "" ]; then
					echo "$MATCHING_INODE " >> "$GLOBAL_DUPLICATE_TABLE" 
				fi
				# add inode number to list in duplicate table
				sed -i "/^$MATCHING_INODE/ s/$/$ino /" "$GLOBAL_DUPLICATE_TABLE"
			fi
		else 
			if [ $MD5_DUPLICATE != 1 ]; then
				echo "$line" >> $GLOBAL_MAIN_TABLE
			else
				# similar to above condition md5 exists 
				if [ "$(grep "$MATCHING_INODE" "$GLOBAL_DUPLICATE_TABLE")" == "" ]; then
					echo "$MATCHING_INODE " >> "$GLOBAL_DUPLICATE_TABLE" 
				fi
				sed -i "/^$MATCHING_INODE/ s/$/$ino /" "$GLOBAL_DUPLICATE_TABLE"
			fi
		fi

		IFS='\n'
	done

	IFS=$'\n'
	for x in $( diff "$OLDNAMEFILE" "$NAMEFILE" --unchanged-line-format="" \
		--old-line-format="%L" --new-line-format="" ); do
		
		# most of code here copy pasted from case of replacement 
		NDUP=$(grep "$ino" "$GLOBAL_DUPLICATE_TABLE" | wc -w)
		if [ $NDUP == 2 ]; then
			# if number of duplicates are two then other file is 
			# no longer a duplicate file
			sed -i "/$ino/d" "$GLOBAL_DUPLICATE_TABLE"
		fi
		sed -i "s/$ino //g" "$GLOBAL_DUPLICATE_TABLE"

		IFS=$ORIGINAL_IFS
		read mod ino name <<< $(echo "$x")
		# I realize that this isn't going to work for hard links
		sed -i "/$ino/d" "$GLOBAL_MAIN_TABLE"
		IFS='\n'
	done

	IFS=$ORIGINAL_IFS

	rm "$OLDNAMEFILE"
	mv "$NAMEFILE" "$OLDNAMEFILE"

	if [ $DEBUG -eq 1 ]; then
		echo "exiting script.sh"
	fi
done 

